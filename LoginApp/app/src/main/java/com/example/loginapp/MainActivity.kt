package com.example.loginapp

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.loginapp.ui.login.LoginActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {

    private val sharedPrefFile = "kotlinsharedpreference"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkForUserSessionToken()
    }

    fun checkForUserSessionToken() {

        var sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        val sessionToken = sharedPref.getString("session_token" , "notoken")

        if (sessionToken != "notoken") {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)

        } else {
            getSessionToken()
        }
    }

    fun getSessionToken() {
        val apiService= UserSessionAPI.create()

        apiService.getToken()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({result->
                saveUserToken(result.userEntryToken)
            },{ error->
                error.printStackTrace()
            })
    }

    fun saveUserToken(token:String?) {
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString("session_token", token)
            commit()
        }
    }
}