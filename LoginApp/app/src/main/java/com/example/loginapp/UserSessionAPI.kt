package com.example.loginapp

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import io.reactivex.Observable

interface UserSessionAPI {
    @GET("rest/session/token")
    fun getToken(): Observable<SessionModel>
    companion object Factory{
        fun create():UserSessionAPI{
            val retrofit= Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://edugaonlabs.com/")
                .build()
            return (retrofit.create(UserSessionAPI::class.java))
        }
    }
}